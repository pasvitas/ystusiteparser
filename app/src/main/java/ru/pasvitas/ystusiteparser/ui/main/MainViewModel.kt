package ru.pasvitas.ystusiteparser.ui.main

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.pasvitas.ystusiteparser.model.NewsItem
import ru.pasvitas.ystusiteparser.services.NewsService
import java.util.concurrent.TimeUnit

class MainViewModel : ViewModel() {
    val newsData : MutableLiveData<List<NewsItem>> = MutableLiveData()

    @SuppressLint("CheckResult")
    fun fetchNewsData() {
        NewsService().getNews()
            .debounce(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { n -> newsData.postValue(n) }
    }
}
