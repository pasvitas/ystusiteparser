package ru.pasvitas.ystusiteparser.ui.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.main_fragment.*
import ru.pasvitas.ystusiteparser.R
import ru.pasvitas.ystusiteparser.adapters.NewsAdapter
import ru.pasvitas.ystusiteparser.model.NewsItem



class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private var isLoading = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRefreshContainer()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        setupLoader()
        setupRefreshContainer()
        fetchNewsData(false)
    }

    private fun fetchNewsData(isRefreshing: Boolean) {
        if (!isLoading) {
            isLoading = true
            if (!isRefreshing) {
                startLoadingAnimation()
            }
            viewModel.fetchNewsData()
        }
    }

    private fun setupLoader() {
        viewModel.newsData.observe(this, Observer {
            isLoading = false
            stopLoadingAnimation()
            swipeContainer.isRefreshing = false
            drawNewsList(it!!)
        })
    }

    private fun setupRefreshContainer() {
        swipeContainer.setOnRefreshListener {
            fetchNewsData(true)
        }
        swipeContainer.setColorSchemeResources(R.color.CornflowerBlue)
    }

    private fun startLoadingAnimation() {
        loading.visibility = View.VISIBLE
        loading.start()
    }

    private fun stopLoadingAnimation() {
        loading.stop()
        loading.visibility = View.GONE
    }

    private fun drawNewsList(items : List<NewsItem>) {
        newsListView.layoutManager = LinearLayoutManager(context)
        val adapter = NewsAdapter(items, context!!)
        newsListView.adapter = adapter
        newsListView.adapter!!.itemCount
    }

}
