package ru.pasvitas.ystusiteparser.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_news.view.*
import ru.pasvitas.ystusiteparser.model.NewsItem
import android.content.Intent
import android.net.Uri
import com.bumptech.glide.Glide
import ru.pasvitas.ystusiteparser.R
import ru.pasvitas.ystusiteparser.services.NewsService.Companion.SITE
import ru.pasvitas.ystusiteparser.services.NewsService.Companion.SITENEWS


class NewsAdapter(private val items : List<NewsItem>, private val context: Context) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_news, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        with(items[position]) {
            holder.title!!.text = title
            holder.date!!.text = date
            holder.link = link

            Glide
                .with(context)
                .load("$SITE$img")
                .centerCrop()
                .placeholder(R.color.White)
                .into(holder.img)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder (view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        val title = view.titleTextView
        val date = view.dateTextView
        val img = view.imageView
        lateinit var link : String

        init {
            title.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(link)))
        }
    }

}