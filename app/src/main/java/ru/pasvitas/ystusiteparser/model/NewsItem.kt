package ru.pasvitas.ystusiteparser.model

data class NewsItem(val title: String, val date: String, val img: String, val link: String)