package ru.pasvitas.ystusiteparser.services

import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.pasvitas.ystusiteparser.model.NewsItem
import org.jsoup.Jsoup
import org.reactivestreams.Subscriber
import java.util.concurrent.Callable


class NewsService {

    fun getNews() : Observable<List<NewsItem>> {
        return Observable.create {
                subscriber ->
            val news = parseNews()
            subscriber.onNext(news)
        }
    }

    private fun parseNews() : List<NewsItem> {
        val newsList = ArrayList<NewsItem>()
        val newsElements = Jsoup.connect(SITENEWS).get().select("section.page-main").select("div").select("a")
        for (element in newsElements) {
            val link = element.attr("href").substring(1)
            var imglink = element.select("span.news-item-image").attr("style")
            imglink = imglink.substring(imglink.indexOf("'")+2, imglink.lastIndexOf("'"))
            newsList.add(
                NewsItem(
                    element.select("span.news-item__title").html(),
                    element.select("span.news-item-image__date").html(),
                    imglink,
                    "$SITE$link"))
        }
        return newsList
    }

    companion object {
        val SITE = "http://www.ystu.ru/"
        val SITENEWS = "http://www.ystu.ru/news/"
    }
}